using Microsoft.Extensions.DependencyInjection;

namespace AzureDevopsHelper.Rest
{
    public static class AzureDevopsHelperRestHelperServiceCollectionExtensions
    {
        public static IServiceCollection AddAzureDevopsRestHelpers(this IServiceCollection services)
        {
            services.AddSingleton<IGitAzureDevopsHelper, GitAzureDevopsHelper>();
            services.AddSingleton(new AzureDevopsBasicAuthentication() {Password = "XXXX"});
            return services.AddHttpClient();
        }
    }
}