namespace AzureDevopsHelper.Rest
{
    public class AzureDevopsBasicAuthentication
    {
        public string UserName { get; } = "Basic";
        public string Password { get; set; }

        public override string ToString()
        {
            return $"{UserName}:{Password}";
        }
    }
}