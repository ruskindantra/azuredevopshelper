using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using AzureDevopsHelper.Poco;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace AzureDevopsHelper.Rest
{
    internal class GitAzureDevopsHelper : IGitAzureDevopsHelper
    {
        private readonly AzureDevopsBasicAuthentication _azureDevopsBasicAuthentication;
        private readonly ILogger<GitAzureDevopsHelper> _logger;
        private readonly IHttpClientFactory _httpClientFactory;

        public GitAzureDevopsHelper(AzureDevopsBasicAuthentication azureDevopsBasicAuthentication, ILogger<GitAzureDevopsHelper> logger, IHttpClientFactory httpClientFactory)
        {
            _azureDevopsBasicAuthentication = azureDevopsBasicAuthentication;
            _logger = logger;
            _httpClientFactory = httpClientFactory;
        }

        public async Task ListRepositories()
        {
            try
            {
                using (var httpClient = _httpClientFactory.CreateClient(/*name*/))
                {
                    var byteArray = Encoding.ASCII.GetBytes(_azureDevopsBasicAuthentication.ToString());
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));

                    //var result = await httpClient.GetAsync("https://dev.azure.com/{{organization}}/{{project}}/_apis/git/repositories?api-version=5.0");

                    var result = await httpClient.GetAsync("https://dev.azure.com/ruskindantra/personal/_apis/git/repositories?api-version=5.0");

                    if (result == null)
                    {
                        _logger.LogWarning("Result was null");
                    }
                    else if (result.Content == null)
                    {
                        _logger.LogWarning("Result.Content was null");
                    }
                    else
                    {
                        var content = await result.Content.ReadAsStringAsync();
                        var obj = JsonConvert.DeserializeObject<Repositories>(content);
                        _logger.LogInformation($"Result is <{content}>");
                    }
                }
            }
            catch (HttpRequestException hre)
            {
                _logger.LogError($"HttpRequestException occurred <{hre.Message}>");
            }
            catch (Polly.CircuitBreaker.BrokenCircuitException bce)
            {
                _logger.LogError($"BrokenCircuitException occurred <{bce.Message}>");
            }
        }
    }
}