using System;

namespace AzureDevopsHelper.Rest
{
    public class AzureDevopsSettings
    {
        public Uri MainUri { get; set; }

        public string Organization { get; set; }

        public string Project { get; set; }

        public string ApiVersion { get; } = "5.0";
    }
}