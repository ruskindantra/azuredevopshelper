using System.Threading.Tasks;

namespace AzureDevopsHelper.Rest
{
    public interface IGitAzureDevopsHelper
    {
        Task ListRepositories();
    }
}