﻿using AzureDevopsHelper.Rest;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace AzureDevopsHelper.Console
{
    internal class App : IHostedService, IDisposable
    {
        private readonly ILogger<App> _logger;
        private readonly IGitAzureDevopsHelper _gitAzureDevopsHelper;

        public App(ILogger<App> logger, IGitAzureDevopsHelper gitAzureDevopsHelper)
        {
            _logger = logger;
            _gitAzureDevopsHelper = gitAzureDevopsHelper;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Starting.");
            _gitAzureDevopsHelper.ListRepositories();

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("Stopping.");
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _logger.LogInformation("Disposing.");
        }
    }

}
