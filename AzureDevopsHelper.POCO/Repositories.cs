using Newtonsoft.Json;

namespace AzureDevopsHelper.Poco
{
    public class Repositories
    {
        [JsonProperty("count")]
        public int Count { get; set; }
        
        [JsonProperty("value")]
        public Repository[] Values { get; set; }
    }
}