﻿using System;
using Newtonsoft.Json;

namespace AzureDevopsHelper.Poco
{
    public class Repository
    {
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("url")]
        public Uri Uri { get; set; }
        
        [JsonProperty("sshUrl")]
        public Uri SshUri { get; set; }
        
        [JsonProperty("webUrl")]
        public Uri WebUri { get; set; }
    }
}